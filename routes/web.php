<?php

use App\Http\Controllers\routerController;
use Illuminate\Support\Facades\Route;

Route::get('/', [routerController::class, 'dashboard']) ->name('dashboard');
Route::get('/dataMahasiswa', [routerController::class, 'dataMahasiswa']) ->name('dataMahasiswa');

Route::get('/input', [routerController::class, 'input']);
Route::post('/inputPost', [routerController::class, 'inputPost']);
Route::get('/edit/{id}', [routerController::class, 'edit']);
Route::post('/update', [routerController::class, 'update']);
Route::get('/delete/{id}', [routerController::class, 'delete']);


