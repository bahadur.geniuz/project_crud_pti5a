@extends('mainlayout')

@section('main')

    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Input Data Mahasiswa</h2>
        </div>

        <div class="row mt-1">

            <div class="col-lg-4">
                <div class="info">
                    <div class="address">
                    <h4>Intruksi:</h4>
                    <p>Sesuaikan Dengan Kartu Tanda Mahasiswa</p>
                    </div>

                    <div class="phone">
                    <h4>CP:</h4>
                    <p>+62 896 1312 7209</p>
                    </div>

                </div>

            </div>

            <div class="col-lg-8 mt-5 mt-lg-0">

                <form action="/inputPost" method="post" >
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" class="form-control" id="nama_mahasiswa" name="nama_mahasiswa" placeholder="Nama Lengkap " autocomplete="off" autofocus >
                        </div>
                        <div class="col-md-6 form-group mt-3 mt-md-0">
                            <input type="text" class="form-control" id="kelas_mahasiswa" name="kelas_mahasiswa" placeholder="Kelas" >
                        </div>
                    </div>

                    <div class="form-group mt-3">
                        <input type="number" class="form-control" id="nim_mahasiswa" name="nim_mahasiswa" placeholder="Nomor Induk Mahasiswa" autocomplete="off" >
                    </div>

                    <div class="form-group mt-3">
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" class="form-control" id="prodi_mahasiswa" name="prodi_mahasiswa" placeholder="Program Studi" >
                        </div>
                        <div class="col-md-6 form-group mt-3 mt-md-0">
                            <input type="text" class="form-control" id="fakultas_mahasiswa" name="fakultas_mahasiswa" placeholder="Fakultas" >
                        </div>
                    </div>

                    <div class="form-group mt-3">
                    </div>
                    
                    <button type="submit" class="btn btn-primary float-right">Submit</button>

                </form>

            </div>

        </div>

        </div>
    </section><!-- End Mahasiswa Section -->



@endsection