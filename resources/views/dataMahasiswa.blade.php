@extends('mainlayout')

@section('main')

    <!-- ======= Data Mahasiswa Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Data Mahasiswa</h2>
        </div>
    
        <div class="container mt-4">
            <div class="row">
                <div class="col-md-10">
                </div>
                <div class="col-md-2">
                    <a href="/input"><button type="button" class="btn btn-primary float-right">+ Add Data</button></a>
                </div>
            </div>
        </div>
    
        <div class="container mt-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NIM</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Prodi</th>
                        <th scope="col">Fakultas</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($mahasiswa as $m)  
                    <tr>
                        <td>{{$m->id}}</td>
                        <td>{{$m->nama_mahasiswa}}</td>
                        <td>{{$m->nim_mahasiswa}}</td>
                        <td>{{$m->kelas_mahasiswa}}</td>
                        <td>{{$m->prodi_mahasiswa}}</td>
                        <td>{{$m->fakultas_mahasiswa}}</td>
                        <td width="200px">
                            <a href="/edit/{{$m->id}}"><button type="button" class="btn btn-success">Change</button></a>
                            <a href="/delete/{{$m->id}}"><button type="button" class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </section><!-- End Data Mahasiswa Section -->

@endsection