@extends('mainlayout')

@section('main')

    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Edit Data Mahasiswa</h2>
        </div>

        <div class="row mt-1">

            <div class="col-lg-4">
            <div class="info">
                <<div class="address">
                    <h4>Intruksi:</h4>
                    <p>Sesuaikan Dengan Kartu Tanda Mahasiswa</p>
                    </div>

                    <div class="phone">
                    <h4>CP:</h4>
                    <p>+62 896 1312 7209</p>
                </div>
            </div>

            </div>

            <div class="container">
                @foreach($mahasiswa as $m)
                    <form action="/update" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="id" value="{{$m->id}}">
                        </div>
                        <div class="form-group">
                            <label for="nama_mahasiswa">Nama</label>
                            <input type="text" class="form-control" id="nama_mahasiswa" name="nama_mahasiswa" placeholder="Masukkan nama mahasiswa " autocomplete="off" autofocus value="{{$m->nama_mahasiswa}}">
                        </div>
                        <div class="form-group">
                            <label for="nim_mahasiswa">NIM</label>
                            <input type="number" class="form-control" id="nim_mahasiswa" name="nim_mahasiswa" placeholder="Masukkan nim mahasiswa" autocomplete="off" value="{{$m->nim_mahasiswa}}">
                        </div>
                        <div class="form-group">
                            <label for="kelas_mahasiswa">Kelas</label>
                            <input type="text" class="form-control" id="kelas_mahasiswa" name="kelas_mahasiswa" placeholder="Masukkan kelas mahasiswa" value="{{$m->kelas_mahasiswa}}">
                        </div>
                        <div class="form-group">
                            <label for="prodi_mahasiswa">Prodi</label>
                            <input type="text" class="form-control" id="prodi_mahasiswa" name="prodi_mahasiswa" placeholder="Masukkan prodi mahasiswa" value="{{$m->prodi_mahasiswa}}">
                        </div>
                        <div class="form-group">
                            <label for="fakultas_mahasiswa">Fakultas</label>
                            <input type="text" class="form-control" id="fakultas_mahasiswa" name="fakultas_mahasiswa" placeholder="Masukkan fakultas mahasiswa" value="{{$m->fakultas_mahasiswa}}">
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </form>
                @endforeach
            </div>

        </div>

        </div>
    </section><!-- End Mahasiswa Section -->



@endsection