<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class routerController extends Controller
{
    public function dashboard()
    {
        return view('dashboard', [
            "title" => "Dashboard"
        ]);
    }

    public function dataMahasiswa()
    {
        $mahasiswa = DB::table('mahasiswa')->get();  
        return view
        (
            'dataMahasiswa', 
            ["title" => "Data Mahasiswa"], 
            ['mahasiswa' => $mahasiswa]
        );
    }

    public function input() 
    {
        $mahasiswa = DB::table('mahasiswa')->get();
        return view
        (
            'input',
            ['mahasiswa' => $mahasiswa],
            ["title" => "Input Data Mahasiswa"],
        );
    }

    public function inputPost(Request $request) 
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa

        ]);
        return redirect('/dataMahasiswa');
    }

    public function edit($id) 
    {
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->get();
        return view
        (
            'edit', 
            ['mahasiswa' => $mahasiswa],
            ["title" => "Edit Data Mahasiswa"]
        );
    }

    public function update(Request $request) {
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa
        ]);
        return redirect('/dataMahasiswa');
    }

    public function delete($id) {
        
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/dataMahasiswa');

    }
}
